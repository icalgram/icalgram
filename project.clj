(defproject icalgram "0.1.0-SNAPSHOT"
  :description "icalgram telegram bot"
  :url "https://gitlab.com/bomjman/icalgram"
  :license {:name "GNU AGPLv3"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 ;; [org.mnode.ical4j/ical4j "3.2.13"]
                 [net.sf.biweekly/biweekly "0.6.8"]
                 [telegrambot-lib "2.12.0"]
                 [cheshire "5.10.1"]
                 [com.taoensso/timbre "6.1.0"]
                 ;; [com.taoensso/carmine "3.2.0"]
                 [com.taoensso/faraday "1.12.0"]
                 [clj-http "3.12.3"]
                 [http-kit "2.6.0"]
                 [compojure "1.7.0"]
                 [ring-cors "0.1.13"]
                 [ring/ring-json "0.5.1"]]
  :repl-options {:init-ns icalgram.core}
  :main  icalgram.core
  :profiles {:uberjar {:aot :all}})
