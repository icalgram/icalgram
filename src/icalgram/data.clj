(ns icalgram.data
  (:require
   [taoensso.faraday :as far])
  (:gen-class))

(def client-opts
  {:access-key (System/getenv "ACCESS_KEY")
   :secret-key (System/getenv "SECRET_KEY")
   :endpoint (System/getenv "ENDPOINT_URL")})

(defn put-event [event]
  (far/put-item client-opts :icalendar event))

(defn get-user-events [user-id]
  (far/query client-opts :icalendar {:user-id [:eq user-id]} {:index :user-id}))

(defn delete-event [event-uuid]
  (far/delete-item client-opts :icalendar {:event-uuid event-uuid}))

