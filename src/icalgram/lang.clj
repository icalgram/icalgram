(ns icalgram.lang
  (:gen-class))

;; (def message-lang
;;   {:summary ["event" "событие"]
;;    :description ["description" "описание"]
;;    :dtstart ["begin" "начало"]
;;    :dtend ["end" "конец"]})

;; (nth (:summary message-lang) (:ru lang))

(def event
  {:summary "Наименование"
   :start-date "Начало"
   :end-date "Конец"
   :description "Описание"})
