(ns icalgram.core
  (:require
   [icalgram.server :as server])
  (:gen-class))

;; (defn ical-parse [file]
;;   (with-open [c (io/input-stream file)]
;;     (.build (CalendarBuilder.) c)))

;; (defn vevents [cal]
;;   (.getComponents cal "VEVENT"))

;; ;; (defn get-ical [url]
;; ;;   (client/get url {:accept "text/calendar" :as :input-stream}))

;; (defn format-date [dt]
;;   (.format (SimpleDateFormat. "HH:mm, dd MMM yyyy" (Locale. "ru" "RU")) dt))

;; (defn date-time [time]
;;   (let [timezone (.getTimeZone time)
;;         value (.getValue time)]
;;     (DateTime. value timezone)))

;; (defn dt? [s]
;;   (some #(= % s) ["DTSTART" "DTEND"]))

;; (defn parse-event [event]
;;   (reduce (fn [acc e]
;;             (assoc acc (-> e .getName .toLowerCase keyword)
;;                    (if (dt? (.getName e))
;;                      (date-time e)
;;                      (.getValue e))))
;;           {} (.getProperties event)))

;; (defn event-values
;;   ([coll] (event-values coll [:summary :description :dtstart :dtend]))
;;   ([coll keys] (select-keys coll keys)))

;; (defn str-event [event lang]
;;   (->> event
;;        (map (fn [[k v]]
;;               (-> (k lang/message-lang)
;;                   (nth lang/lang)
;;                   (str ": " v "\n"))))
;;        (apply str)))

;; (defn update-map [m ks f]
;;   (reduce (fn [acc k]
;;             (assoc acc k (-> acc k f)))
;;           m ks))

;; (let [file "/home/bombox/Downloads/yandex-cal.ics"
;;       events (-> file ical-parse vevents)]
;;   (for [e events]
;;     (-> e
;;         parse-event
;;         event-values
;;         (update-map [:dtstart :dtend] format-date)
;;         (str-event 1))))

;; #(print-event % 0)

;; TODO
;; filter outdated events
;; if data have been parsed then cache ical data in redis
;; store ical events in mariadb
;; make telegram frontend (buttons, text, emoji)
;; parse icals via http every n time

(defn -main []
  ;; run bot
  ;; (future (app bot))
  ;; run backend server
  (server/run-server))
