(ns icalgram.messages
  (:gen-class))

(defn chat-id [msg]
  (-> msg
      :message
      :chat
      :id))

(defn text [msg]
  (-> msg
      :message
      :text))

(defn reply-text [msg]
  (-> msg
      :message
      :reply_to_message
      :text))

(defn user-id [msg]
  (-> msg
      :message
      :from
      :id))

(defn message-id [msg]
  (-> msg
      :message
      :message_id))

(defn username [msg]
  (-> msg
    :message
    :chat
    :username))

(defn callback-query [msg]
  (-> msg
      :callback_query))

(defn document [msg]
  (-> msg
      :message
      :document))
