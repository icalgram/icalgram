(ns icalgram.bot
  (:require
   [icalgram.messages :as msg]
   [icalgram.lang :as lang]
   [telegrambot-lib.core :as tbot :refer [send-message]])
  (:gen-class))

(def my-bot (tbot/create (System/getenv "BOT_TOKEN")))

(defn answer-msg [bot msg {:keys [text params]}]
  (tbot/send-message bot (msg/chat-id msg) text params))

(defn commands
  "telegram bot's commands handler"
  [bot msg]
  (->> (case (msg/text msg)
         "/start" {:text "Приветствую! Используйте меню чтобы создать событие"}
         {:text "команда не найдена"})
       (answer-msg bot msg)))

;; (defn get-ical
;;   ([url] (:body (client/get url {:accept "text/calendar" :as :stream})))
;;   ([bot doc]
;;    (let [file-path (->> doc :file_id (tbot/get-file bot) :result :file_path)
;;          url (str "https://api.telegram.org/file/bot" (:bot-token bot) "/" file-path)]
;;      (:body (client/get url {:as :stream})))))

;; (defn url? [url]
;;   (.isValid (UrlValidator.) url))

(defn format-new-event [coll]
  (->>
   coll
   (keep
    (fn [[k v]]
      (when (and (k lang/event) v)
        (format "%s: %s" (k lang/event) v))))
   (interpose "\n")
   (apply str "Новое событие\n")))

(defn event-notify [event chat-id]
  (->> event
       format-new-event
       (send-message my-bot chat-id)))

(defn handler [msg]
  (let [bot my-bot
        callback (msg/callback-query msg)
        message (msg/text msg)
        reply (msg/reply-text msg)
        user-id (-> msg msg/user-id)
        ;; save-input (fn [k] (data/update-input-data user-id  {k message}))
        callback-data (-> callback :data keyword) ]

    ;; handle messages
    (cond
      ;; (url? message) (parse-events (get-ical bot message))

      ;; handle documents
      ;; (when-let [document (msg/document msg)]
      ;;   (cond
      ;;     (= "text/calendar" (:mime_type document)) (parse-events (get-ical bot document))))

      ;; commands
      (re-find #"^/" message) (commands bot msg)

      :else (answer-msg bot msg {:text "Я вас не понимаю"}))))
