(ns icalgram.server
  (:require
   [compojure.core :refer [defroutes GET POST DELETE]]
   [compojure.route :refer [not-found]]
   [ring.middleware.cors :refer [wrap-cors]]
   [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
   [icalgram.bot :as tbot]
   [icalgram.create :as ical]
   [icalgram.data :as data]
   [org.httpkit.server :as hk-server]
   [taoensso.timbre :as log])
  (:gen-class))

(defn new-event [{:keys [body] :as req}]
  (log/info body)
  (data/put-event (ical/new-ical body))
  {:status 200})

(defn alarm-users [{:keys [body] :as req}]
  (log/info body)
  (ical/notify body))

(defn bot-handler [{:keys [body] :as req}]
  (log/info body)
  (tbot/handler body))

(defn get-events [user-id]
  {:status 200
   :headers {"Content-Type" "application/json; charset=utf-8"}
   :body (-> user-id
             Integer/parseInt
             data/get-user-events
             ical/parse-icals)})

(defn delete-event [event-uuid]
  (data/delete-event event-uuid)
  {:status 200})

(defroutes router
  (GET "/events/:user-id" [user-id] (get-events user-id))
  (POST "/bot" [] bot-handler)
  (POST "/notifyEvents" [] alarm-users)
  (POST "/newEvent" [] new-event)
  (DELETE "/event/:event-uuid" [event-uuid] (delete-event event-uuid))
  (not-found (fn [req] (prn req) {:status 404})))

(def handler
  (->
   router
   (wrap-json-body {:keywords? true})
   wrap-json-response
   (wrap-cors :access-control-allow-origin [#".*"]
              :access-control-allow-methods [:get :post :delete])))

(defonce server (atom nil))

(defn run-server []
  (if-let [port (System/getenv "PORT")]
    (do (log/info "Server has been started")
        (reset! server (hk-server/run-server handler {:port (Integer/parseInt port)
                                                     :error-logger #(log/error %)
                                                     :warn-logger #(log/warn %)})))
    (log/error "The PORT env variables is not defined")))
