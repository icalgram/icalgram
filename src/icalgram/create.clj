(ns icalgram.create
  (:import [biweekly ICalendar Biweekly]
           [biweekly.property Uid]
           [biweekly.component VEvent]
           [java.util Date]
           [java.time OffsetDateTime]
           [java.time.format DateTimeFormatter])
  (:gen-class)
  (:require
    [icalgram.bot :as tbot]))

(defn parse-date
  ([date] (parse-date date false))
  ([[date] string?]
   (let [patern (DateTimeFormatter/ofPattern "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
         dt (-> date
                (OffsetDateTime/parse patern)
                (.toInstant))]
     (if string?
       (str dt)
       (Date/from dt)))))

;; https://github.com/mangstadt/biweekly/wiki/Examples
(defn add-event
  "Create ical event"
  [{:keys [summary start-date end-date description event-attendees uuid]}]
  (let [event (VEvent.)]
    (.setSummary event summary)
    (.setDateStart event start-date)
    (.setDateEnd event end-date)
    (.setUid event uuid)
    (when description
      (.setDescription event description))
    event))

(defn add-ical
  ([event] (add-ical (ICalendar.) event))
  ([ical event] (.addEvent ical event) ical))

;; (defn ical->jcal [ical]
;;   (with-open [out (ByteArrayOutputStream.)
;;               writer (JCalWriter. out)]
;;     (.write writer ical)
;;     out))

(defn new-ical [{:keys [start-date user-id event-uuid] :as coll}]
  (let [event (reduce #(update %1 %2 parse-date) coll [:start-date :end-date])
        uuid (if event-uuid (Uid. event-uuid) (Uid/random))]
    {:user-id user-id
     :start-time (parse-date start-date true)
     :event-uuid (.getValue uuid)
     :ical (-> event
               (assoc :uuid uuid)
               add-event
               add-ical
               .write)}))

(defn parse-props [ical]
  (let [event (first (.getEvents ical))]
      {:summary     (-> event .getSummary .getValue)
       :description (some-> event .getDescription .getValue)
       :start-date  (-> event .getDateStart .getValue)
       :end-date    (-> event .getDateEnd .getValue)
       ;:attendees   (some-> event .getAttendees)
       }))

(defn parse-ical [ical]
  (->
   (:ical ical)
   Biweekly/parse
   .first
   parse-props))

(defn parse-icals [icals]
  (map #(merge (parse-ical %)
               (select-keys % [:event-uuid]))
       icals))

(defn notify [coll]
  (doseq [ical coll]
    (-> ical
        parse-ical
        (tbot/event-notify (:user-id ical)))))
