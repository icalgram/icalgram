FROM eclipse-temurin:11-alpine
RUN addgroup -S java && adduser -S java -G java
USER java:java
WORKDIR /app
COPY --chown=java:java target/*-standalone.jar app.jar
CMD ["java", "-jar", "app.jar"]
